import data.Customers;
import servlet.ConnectionService;
import java.util.Scanner;
import services.Users;

public class Application {
    public static void main(String[] args){
        ConnectionService connectionService = new ConnectionService();
        Users users = new Users();

        users.customers = connectionService.updateCustomer();
        Customers customers = new Customers();
        customers.menu();

    }
}
