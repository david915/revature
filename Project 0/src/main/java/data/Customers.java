package data;

import services.Users;
import java.util.Scanner;
import servlet.ConnectionService;

public class Customers {
//Holds customer options

    boolean isExit = false;
    Users users = new Users();
    ConnectionService cs = new ConnectionService();


    public void menu(){
        isExit = false;
        Scanner ask = new Scanner(System.in);
        users.customers = cs.updateCustomer();
        users.cashAmmount = cs.updateCashAmmount();

        System.out.println("Select an option: ");
        System.out.println("");
        System.out.println("1 - Login");
        System.out.println("2 - Create new account");
        System.out.println("3 - Exit");

        int x = ask.nextInt();

        switch(x){
            case 1:
                System.out.println("Enter your accountID: ");
                String accountID = ask.next();
                System.out.println("Enter your password");
                String password = ask.next();
                if(users.isCheckCorrect(accountID, password)) {
                    System.out.println("");
                    System.out.println("Welcome");
                    System.out.printf("Your current balance is: $%,.02f%n", users.balance(accountID));
                    System.out.println("");
                    System.out.println("1 - Deposit");
                    System.out.println("2 - Withdraw");
                    System.out.println("3 - Logout");

                    while(!isExit){
                        int b = ask.nextInt();

                        if (b == 1) {
                            users.Deposits(accountID);
                            cs.updateCash(accountID,users.balance(accountID));
                            System.out.println("1 - Deposit");
                            System.out.println("2 - Withdraw");
                            System.out.println("3 - Logout");

                        } else if (b == 2) {
                            users.Withdraws(accountID);
                            cs.updateCash(accountID,users.balance(accountID));
                            System.out.println("1 - Deposit");
                            System.out.println("2 - Withdraw");
                            System.out.println("3 - Logout");

                        } else {
                            isExit = true;
                            System.out.println("Logging out.......");
                            menu();
                        }
                    }
                }

                else{
                    System.out.println("The username or password is wrong");
                    menu();
                }
                break;

            case 2:
                System.out.println("create a username and password;");
                accountID = ask.next();
                password = ask.next();
                if(users.customers.containsKey(accountID)){
                    System.out.println("");
                    System.out.println("please create a new username");
                    menu();
                }
                else {
                    System.out.println("");
                    System.out.println("Your account has been created");
                    cs.insertAccount(accountID,password);
                    cs.updateCustomer();
                    cs.updateCashAmmount();
                    menu();
                    break;
                }
            case 3:
                isExit = true;
                break;
        }
    }

}
