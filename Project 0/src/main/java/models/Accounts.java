package models;
//Main variables

public class Accounts {
    private String accountID;
    private double cash = 0.00;
    private String password;
    private double balance;

    public String getAccountID() {
        return this.accountID;
    }

    public void setAccountID(String accountID){
       this.accountID = accountID;
    }

    public double getCash() {
        return this.cash;
    }

    public void setCash(double cash) {
        this.cash = cash;
    }

    public void setPassword(String password) {
       this.password = password;
    }

    public void setBalance(){
        this.balance = this.balance + getCash();
    }

}
