package services;

import models.Accounts;
import java.util.Scanner;
import java.util.HashMap;
import java.util.Objects;
import servlet.ConnectionService;

public class Users {
    public HashMap<String,String> customers = new HashMap<String,String>();
    public HashMap<String, Double> cashAmmount = new HashMap<String, Double>();
    Accounts accounts = new Accounts();
    Scanner ask = new Scanner(System.in);
    //ConnectionService cs = new ConnectionService();

    public void createAccount(String accountID){
        accounts.setAccountID(accountID);
    }

    public void createPassword(String password){
        accounts.setPassword(password);
    }

    public double balance(String username){
        return cashAmmount.get(username);
    }

    public void setDeposit(String username, double ammount){
        double current = cashAmmount.get(username);
        current = current + ammount;
        cashAmmount.replace(username,ammount);
    }

    public void setWithdraw(String username, double ammount){
        double current = cashAmmount.get(username);
        current = current - ammount;
        cashAmmount.replace(username,ammount);
    }

    public double deposit(double deposit,String username){
        double balance = this.balance(username);
        return balance + deposit;
    }

    public double withdraw(double withdraw,String username){
        double balance = balance(username);
        return balance - withdraw;
    }

    public void newAccount(String username,String password){
        createAccount(username);
        createPassword(password);
        customers.put(username,password);
        cashAmmount.put(username,0.00);
    }

    public boolean isCheckCorrect(String username,String password){
        if(Objects.equals(customers.get(username), password)){
            return true;
        }
        else
        return false;
    }

    public void Deposits(String accountID){
        System.out.println("How much would you like to deposit?");
        double deposit = ask.nextDouble();
        if(deposit<0){
            System.out.println("Sorry invalid input");
        }
        else {
            double newDeposit = deposit(deposit, accountID);
            setDeposit(accountID, newDeposit);
            System.out.println("");
            System.out.printf("Your new balance is: $%,.02f%n%n", balance(accountID));
            System.out.println("");
        }
    }

    public void Withdraws(String accountID){
        System.out.println("How much would you like to withdraw?");
        double withdraw = ask.nextDouble();

        if (balance(accountID) < withdraw) {
            System.out.println("");
            System.out.println("Insufficient Funds");
            System.out.println("");
        }

        else {
            double newWithdraw = withdraw(withdraw, accountID);
            setWithdraw(accountID, newWithdraw);
            System.out.println("");
            System.out.printf("Your new balance is: $%,.02f%n",balance(accountID));
            System.out.println("");
        }
    }
}
