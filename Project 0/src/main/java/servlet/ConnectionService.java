package servlet;

import services.Users;
import java.sql.*;
import services.Users;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import data.Customers;

public class ConnectionService {
    Scanner ask = new Scanner(System.in);
    public HashMap<String,String> customers = new HashMap<String,String>();
    public HashMap<String, Double> cashAmmount = new HashMap<String, Double>();
    Users users = new Users();

    public void insertAccount(String accountID,String pass) {
        try {
            String url = "jdbc:postgresql://localhost:5432/postgres";
            String username = "postgres";
            String password = "Password00";
            Connection sts = DriverManager.getConnection(url, username, password);
            String query = "insert into customer_information values (?,?,0.00)";
            PreparedStatement preparedStatement = sts.prepareStatement(query);
            preparedStatement.setString(1,accountID);
            preparedStatement.setString(2,pass);
            preparedStatement.executeUpdate();
            updateCustomer();
            updateCashAmmount();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public HashMap<String,String> updateCustomer() {
        try {
            String url = "jdbc:postgresql://localhost:5432/postgres";
            String username = "postgres";
            String password = "Password00";
            Connection sts = DriverManager.getConnection(url, username, password);
            Statement statement = sts.createStatement();
            ResultSet results = statement.executeQuery("SELECT * FROM customer_information");
            while (results.next()){
                String name = results.getString("customer_username");
                String pass = results.getString("customer_password");
                customers.put(name,pass);
            }
            for(Map.Entry<String,String> entry:customers.entrySet()){
                users.customers.put(entry.getKey(),entry.getValue());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users.customers;
    }

    public HashMap<String,Double> updateCashAmmount() {
        try {
            String url = "jdbc:postgresql://localhost:5432/postgres";
            String username = "postgres";
            String password = "Password00";
            Connection sts = DriverManager.getConnection(url, username, password);
            Statement statement = sts.createStatement();
            ResultSet results = statement.executeQuery("SELECT * FROM customer_information");
            while (results.next()){
                String name = results.getString("customer_username");
                double money = results.getDouble("customer_money");
                cashAmmount.put(name,money);
            }
            for(Map.Entry<String,Double> entry:cashAmmount.entrySet()){
                users.cashAmmount.put(entry.getKey(),entry.getValue());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users.cashAmmount;
    }

    public void newAccount(){
        try {
            String url = "jdbc:postgresql://localhost:5432/postgres";
            String username = "postgres";
            String password = "Password00";
            Connection sts = DriverManager.getConnection(url, username, password);
            Statement statement = sts.createStatement();
            ResultSet results = statement.executeQuery("SELECT * FROM customer_information");
            while (results.next()){
                String name = results.getString("customer_username");
                String pass = results.getString("customer_password");
                String money = results.getString("customer_money");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateCash(String accountID,double cash){
        try {
            String url = "jdbc:postgresql://localhost:5432/postgres";
            String username = "postgres";
            String password = "Password00";
            Connection sts = DriverManager.getConnection(url, username, password);
            String query = ("update customer_information set customer_money = ? where customer_username = ?");
            PreparedStatement preparedStatement = sts.prepareStatement(query);
            preparedStatement.setDouble(1,cash);
            preparedStatement.setString(2,accountID);
            preparedStatement.executeUpdate();
            sts.close();
            updateCustomer();
            updateCashAmmount();
            }
         catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
